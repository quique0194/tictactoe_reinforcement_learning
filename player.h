#ifndef PLAYER_H
#define PLAYER_H

class Player{
public:
    Player(char _symbol): symbol(_symbol){}
    virtual void win() = 0;
    virtual void lose() = 0;
    virtual void draw() = 0;
    virtual void move(Board &board) = 0;  
    char symbol;
};

class Human: public Player{
public:
    Human(char _symbol): Player(_symbol){}
    void win(){
        cout << "You won" << endl;
    }
    void lose(){
        cout << "You lose" << endl;
    }
    void draw(){
        cout << "Draw" << endl;
    }
    void move(Board &b){
        bool play = false;
        while(!play){
            cout << "Siguiente jugada: ";
            int pos; cin >> pos;
            play = b.play(symbol, pos);
        }
    }
};

#endif // PLAYER_H