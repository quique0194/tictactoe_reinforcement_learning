#ifndef IA_H
#define IA_H

#include <iostream>
#include <fstream>
#include <stack>
#include <cstdlib>
using namespace std;

#include "player.h"

#define V_SIZE 19683

class IAValues{
public:
    IAValues(){
        for(int i=0; i<V_SIZE; ++i)
            V[i] = 0;
    }
    void save(const char* filename){
        ofstream file(filename, ios::binary | ios::trunc);
        file.write((char*)V, sizeof(V));
        file.close();
    }
    // returns true if load successful, otherwise returns false
    bool load(const char* filename){
        ifstream file(filename, ios::binary);
        if(file.good()){
            file.read((char*)V, sizeof(V));
            file.close();
            return 1;
        }
        else{
            file.close();
            return 0;
        }

    }
    void set(int pos, double val){
        V[pos] = val;
    }
    double get(int pos){
        return V[pos];
    }
private:
    double V[V_SIZE];
};

class Policy{
public:
    Policy(IAValues* _V):V(_V){}
    virtual Board choose(const vector<Board> &boards) = 0;
protected:
    IAValues* V;
};

class BoltzmanPolicy: public Policy{
public:
    BoltzmanPolicy(IAValues* _V, double _temperature): Policy(_V), temperature(_temperature){}
    Board choose(const vector<Board> &boards){
        double total = 0;
        for(int i=0; i<boards.size(); ++i) 
            total += aux(boards[i]);
        double random = (double)rand()/RAND_MAX;
        // cout << "random: " << random << endl;
        double accumulated = 0;
        for(int i=0; i<boards.size(); ++i){
            accumulated += aux(boards[i])/total;
            // cout << "acumulated: " <<accumulated<<endl;
            if(accumulated>random)
                return boards[i];
        }
        return boards.back();
    }
private:
    double temperature;
    double aux(const Board &b){
        return exp(V->get(b.hash())/temperature);
    }
};

class EGreedePolicy: public Policy{
public:
    EGreedePolicy(IAValues* _V, double _e): Policy(_V), e(_e){}
    Board choose(const vector<Board> &boards){
        double random = (double)rand()/RAND_MAX;
        if(random < e)
            return random_choose(boards);
        else
            return greedy_choose(boards);
    }
private:
    double e;
    Board greedy_choose(const vector<Board> &boards){
        int ret = 0;
        double maxv = V->get(boards[ret].hash());
        // cout << "====================================" << endl;
        for(int i=0; i<boards.size(); ++i){
            // cout << V->get(boards[i].hash()) << " ";
            if(maxv < V->get(boards[i].hash())){
                maxv = V->get(boards[i].hash());
                ret = i;
            }
        }
        // cout << endl;
        return boards[ret];
    }
    Board random_choose(const vector<Board> &boards){
        int pos = rand()%boards.size();
        return boards[pos];
    }
};


class IA: public Player{
public:
    IA(char _symbol, IAValues* _V, Policy* _policy):Player(_symbol), V(_V), policy(_policy){}
    void move(Board& b){
        vector<Board> plays = b.generate_possible_plays(symbol);
        Board next_play = policy->choose(plays);
        last_plays.push(next_play.hash());
        b = next_play;
    }
    void win(){
        reward(1);
    }
    void lose(){
        reward(-1);
    }
    void draw(){
        reward(0);
    }
    IAValues* V;
    Policy* policy;
private:
    stack<int> last_plays;
    double alpha(){
        return 0.5;
    }    
    void reward(double reward){
        V->set(last_plays.top(), reward);
        int next_play = last_plays.top();
        last_plays.pop();
        while(!last_plays.empty()){
            int play = last_plays.top();
            last_plays.pop();
            // cout << "V: " << V->get(play) << endl;
            double delta = alpha()*(V->get(next_play) - V->get(play));
            V->set(play, V->get(play)+delta);
            next_play = play;
        }
    }
};

#endif // IA_H