#include <ctime>

#include "board.h"
#include "ia.h"
#include "player.h"

void game(Player* p1, Player* p2, bool display){
    p1->symbol = 'X';
    p2->symbol = 'O';
    Board b;
    while(1){
        if(display) b.display();
        if(b.game_over()){
            p1->lose();
            p2->win();
            break;
        }
        else if(b.board_full()){
            p1->draw();
            p2->draw();
            break;
        }
        p1->move(b);

        if(display) b.display();
        if(b.game_over()){
            p1->win();
            p2->lose();
            break;
        }
        else if(b.board_full()){
            p1->draw();
            p2->draw();
            break;
        }
        p2->move(b);
    }
}

int main(){
    srand(time(0));
    IAValues iavalues;
    // BoltzmanPolicy boltzman(&iavalues, 10);
    // IA ia1('X', &iavalues, &boltzman);
    // IA ia2('O', &iavalues, &boltzman);
    EGreedePolicy egreedy(&iavalues,0.05);
    IA ia1('X', &iavalues, &egreedy);
    IA ia2('O', &iavalues, &egreedy);
    if(!iavalues.load("iavalues.bin")){
        int cycles = 100000;
        while(cycles--)
            game(&ia1, &ia2, false);
        iavalues.save("iavalues.bin");
    }

    EGreedePolicy egreedy0(&iavalues,0);
    ia1.policy = &egreedy0;
    ia2.policy = &egreedy0;
    Human human('X');
    while(1){

        char option = ' ';
        while(option!='d' && option!='1' && option!='2' && option!='q'){
            cout << "What's next? (d:demo, 1:player1, 2:player2, q:quit): ";
            cin >> option;
        }

        if(option=='d'){
            game(&ia1, &ia2, true);
        }
        else if(option=='1'){
            game(&human, &ia1, true);
        }
        else if(option=='2'){
            game(&ia1, &human, true);
        }
        else if(option=='q'){
            iavalues.save("iavalues.bin");
            break;
        }
    }
    
    return 0;
}