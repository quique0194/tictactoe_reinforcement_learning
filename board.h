#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#include <vector>
#include <cmath>
#include <cstring>
using namespace std;

class Board{
public:
    Board(){
        for(int i=0; i<9; ++i)
            board[i] = ' ';
    }
    void display(){
        for(int i=0; i<3; ++i){
            cout << "|";
            for(int j=0; j<3; ++j){
                cout << board[i*3+j] << "|";
            }
            cout << endl;
        }
        cout << endl;
    }
    bool play(char symbol, int pos){ // symbol: 'X' or 'O'; pos: 0,1,..,8
        if(board[pos]==' '){
            board[pos] = symbol;
            return 1;
        }
        return 0;
    }
    vector<Board> generate_possible_plays(char symbol) const{
        vector<Board> ret;
        for(int i=0; i<9; ++i)
            if(board[i]==' '){
                Board b;
                if(!memcpy(b.board, board, sizeof board))
                    cout << "CRAP!" << endl;
                b.board[i]=symbol;
                ret.push_back(b);
            }
        return ret;
    }
    // There is a winner or a looser
    bool game_over(){
        for(int i=0; i<3; ++i)
            if(row(i) || col(i)) return 1;
        if(diag1() || diag2()) return 1;
        return 0;
    }
    // Draw
    bool board_full(){
        for(int i=0; i<9; i++)
            if(board[i]==' ') return false;
        return true;
    }
    // Returns a number between [0;19683>
    // representing each board uniquely
    int hash() const{
        int ret = 0;
        for(int i=0; i<9; ++i)
            if(board[i]==' ') ret += 0;
            else if(board[i]=='X') ret += pow(3,i);
            else ret += 2*pow(3,i);
        return ret;
    }
    // Print instructions of how to play
    static void how_to_play(){
        cout << "HOW TO PLAY" << endl;
        cout << "In your turn you must enter a number" << endl;
        cout << "These are the positions of the board" << endl;
        for(int i=0; i<3; ++i){
            cout << "|";
            for(int j=0; j<3; ++j){
                cout << i*3+j << "|";
            }
            cout << endl;
        }
        cout << endl;
    }
private:
    char board[9];

    // Tests if there is a wining position in row i
    bool row(int i){    // i: 0, 1 or 2
        return board[3*i]==board[3*i+1] && 
            board[3*i]==board[3*i+2] && 
            board[3*i]!=' ';
    }
    // Tests if there is a wining position in column i
    bool col(int i){    // i: 0, 1 or 2
        return board[i]==board[3+i] && 
            board[i]==board[6+i] && 
            board[i]!=' ';
    }
    // Tests if there is a wining position in main diagonal
    bool diag1(){
        return board[0]==board[4] &&
            board[0]==board[8] &&
            board[0]!=' ';
    }
    // Tests if there is a wining position in secondary diagonal
    bool diag2(){
        return board[2]==board[4] &&
            board[2]==board[6] &&
            board[2]!=' ';
    }
};

#endif // BOARD_H